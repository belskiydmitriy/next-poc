import { useState, useEffect } from 'react'
import Button from '../components/button';
import Layout from '../components/layout'

export default function Page () {
  const [ todoNumber , setTodoNumber ] = useState()
  const [ content , setContent ] = useState()

  useEffect(()=>{
    const fetchData = async () => {
      const res = await fetch(`/api/todos/${todoNumber}`)
      const json = await res.json()
      if (json.id) { setContent(json) }
    }
    fetchData()
  },[todoNumber])
  
  return (
    <Layout>
      <h1>Choose{' '}
        <Button onClick={() => setTodoNumber(1)}>1</Button>
        {' '}or{' '}
        <Button onClick={() => setTodoNumber(2)}>2</Button>
        {' '}or{' '} 
        <Button onClick={() => setTodoNumber(3)}>3</Button>
      </h1>
      <hr />
      <h3>UserId: {content?.userId}</h3>
      <h3>Id: {content?.id}</h3>
      <h3>Title: {content?.title}</h3>
      <h3>Completed: {content?.completed.toString()}</h3>
    </Layout>
  )
}