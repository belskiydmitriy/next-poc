import { useState, useEffect } from 'react'
import Button from '../components/button';
import Layout from '../components/layout'

export default function Page () {
  const [ jobsNumber , setJobsNumber ] = useState()
  const [ content , setContent ] = useState()

  useEffect(()=>{
    const fetchData = async () => {
      const res = await fetch(`/api/jobs/${jobsNumber}`)
      const json = await res.json()
      if (json.id) { setContent(json) }
    }
    fetchData()
  },[jobsNumber])
  
  return (
    <Layout>
      <h1>Choose{' '}
        <Button onClick={() => setJobsNumber(1)}>1</Button>
        {' '}or{' '}
        <Button onClick={() => setJobsNumber(2)}>2</Button>
        {' '}or{' '} 
        <Button onClick={() => setJobsNumber(3)}>3</Button>
      </h1>
      <hr />
      <h3>Id: {content?.id}</h3>
      <h3>Position: {content?.title}</h3>
      <h3>Company: {content?.company}</h3>
      <h3>Location: {content?.location}</h3>
    </Layout>
  )
}