import Layout from "../components/layout"
import { TileImage } from "../components/tile-image"

export default function Page () {
  return (
    <Layout>
      <h1>Just a sample of an Image</h1>
      <TileImage />
    </Layout>
  )
}