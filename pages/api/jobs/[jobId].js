import { jobs } from '../../../data.js'

export default async (req, res) => {
  const jobId = req.query.jobId;
  const result = jobs. filter((job) => job.id === parseInt(jobId))

  if(result.length > 0) {
    res.status(200).json(result[0]);
  } else {
    res.status(404).json({ message: `Job with id: ${jobId}`});
  }
}