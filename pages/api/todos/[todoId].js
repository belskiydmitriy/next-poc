export default async (req, res) => {
  const todoId = req.query.todoId;
  const result = await fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
  const json = await result.json()

  if(json) {
    res.status(200).json(json);
  } else {
    res.status(404).json({ message: `Todo with id: ${jobId} not found`});
  }
}