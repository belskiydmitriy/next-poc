import Image from 'next/image'

export const TileImage = () => (
  <Image
    src="/images/tile.jpg" // Route of the image file
    height={144} // Desired size with correct aspect ratio
    width={144} // Desired size with correct aspect ratio
    alt="Tile"
  />
)