import Link from 'next/link'
import styles from './header.module.css'
import SignIn from './singin'

export default function Header () {
  return (
    <header>
      <SignIn />
      <nav>
        <ul className={styles.navItems}>
          <li className={styles.navItem}><Link href="/"><a>Home</a></Link></li>
          <li className={styles.navItem}><Link href="/client"><a>Client</a></Link></li>
          <li className={styles.navItem}><Link href="/client-protected"><a>Client-Protected</a></Link></li>
          <li className={styles.navItem}><Link href="/server"><a>Server</a></Link></li>
          <li className={styles.navItem}><Link href="/protected"><a>Protected</a></Link></li>
          <li className={styles.navItem}><Link href="/api-example"><a>API</a></Link></li>
          <li className={styles.navItem}><Link href="/image"><a>Image</a></Link></li>
          <li className={styles.navItem}><Link href="/jobs"><a>Jobs</a></Link></li>
          <li className={styles.navItem}><Link href="/todos"><a>Todos</a></Link></li>
        </ul>
      </nav>
    </header>
  )
}
