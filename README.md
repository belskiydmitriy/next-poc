## PoC (Proof of Concept) на Next.js
> *проверяем следующие концепты:*
### Возможность проксирования запросов к внешним сервисам через API Routes
Идем на `http://localhost:3000/todos`. Выбираем номер todo. Отправляется запрос `fetch("/api/todos/${todoNumber}")`.
Оттуда запрос перенаправляется на внешний сервис `fetch("https://jsonplaceholder.typicode.com/todos/${todoId}")`.  
В `http://localhost:3000/jobs` похожий функционал, но запрос идет к файлу `data.js` в корне приложения.
### Возможность разделение публичных и приватных частей приложения
`http://localhost:3000/client-protected` и `http://localhost:3000/protected` - приватные части. 
По остальным путям находится публичный контент.
### Возможность авторизация через Google
UI авторизации вынесен в отдельный компонент `components/signin.js`.  
`GOOGLE_ID` и `GOOGLE_SECRET` в `env.local` - невалидные, только для примера
### Возможность генерации HTML страниц на сервере (SSR)
Реализовано
### Вопрос
Пока не получилось разобраться где и как нужно использовать функцию `getServerSideProps`
*pages/server.js*
```
export async function getServerSideProps(context) {
  return {
    props: {
      session: await getSession(context)
    }
  }
}
```
